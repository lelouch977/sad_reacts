# Create Image from node image
FROM node:8.6-alpine

#Create a custom app path to put your files in
WORKDIR /var/app

#Copy Package.json and install node modules
COPY ./package.json /var/app/package.json
RUN npm install


#Copy Everything else
COPY ./ /var/app/


#Set ENV Variables before running
ENV NODE_ENV development
ENV PORT 3000


#EXPOSE the port that your app will listen to.
#The container will not listen to any other port 
#that is not exposed.
EXPOSE 3000


#Start your application
CMD npm start
