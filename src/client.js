import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './routes'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from './redux/reducers'
import sagas from './redux/saga'

import createSocketIoMiddleware from 'redux-socket.io'
import io from 'socket.io-client'
const socket = io('http://localhost:3000')

require('../sass/main.scss')

const socketIoMiddleware = createSocketIoMiddleware(socket, 'server/')
const sagaMiddleware = createSagaMiddleware()

let store = createStore(
  reducers,
  window.STATE_FROM_SERVER,
  applyMiddleware(sagaMiddleware, socketIoMiddleware)
)

sagaMiddleware.run(sagas)

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>, document.getElementById('root'))
