import React, { Component } from 'react'
import { connect } from 'react-redux'
import actions from '../redux/actions'

import { Button, Snackbar } from 'react-md'

class Index extends Component {
  constructor() {
    super()
    this.state = {
      toasts: []
    }
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props
    if (nextProps.error.message) {
      this.addToast({ text: nextProps.error.message })
      dispatch(actions.ClearMessage())
    }
  }

  addText() {
    this.props.dispatch(actions.AddText())
  }

  UserList() {
    this.props.dispatch(actions.UserList())
  }

  testSocket() {
    this.props.dispatch(actions.TestSocket())
  }

  addToast(item) {
    const { toasts } = this.state
    toasts.push(item)
    this.setState({ toasts })
  }

  dismissToast() {
    const [, ...toasts] = this.state.toasts
    this.setState({ toasts })
  }

  render() {
    const { toasts } = this.state

    return (<div>
      <p className='sample'>hi from index</p>
      <Button
        onClick={::this.addText}
        flat
        secondary
      >add text</Button>

      {this.props.error.texts.map((item, key)=>{
      	return (
      	<p key={key}>{item}</p>
      	)
      })}

      <Button
        onClick={::this.UserList}
        flat
        secondary
      >get list</Button>

      {this.props.error.users.map((item, key)=>{
        return (
          <p key={key}>{item}</p>
        )
      })}

      <Button
        onClick={::this.testSocket}
        flat
        primary
      >Emit Socket event</Button>

      {
        toasts.map((item, key)=>{
          return <p key={key}>{item.text}</p>
        })
      }

      <Snackbar
        id='toast_messages'
        toasts={toasts}
        autohide={true}
        portal={true}
        onDismiss={::this.dismissToast}
      />

    </div>)
  }
}
export default connect(state=>(state))(Index)
