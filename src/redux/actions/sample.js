import {
  CLEAR_ERROR,
  REQUESTED
} from '../types'

module.exports = {
  ClearError() {
    return {
      type: CLEAR_ERROR,
      payload: null
    }
  },

  AddText() {
  	return {
  		type: 'ADD_TEXT'
  	}
  },

  UserList() {
    return {
      type: 'USER_LIST' + REQUESTED
    }
  },

  TestSocket() {
    return {
      type: 'server/fuck',
      data: 'haha'
    }
  },

  ClearMessage() {
    return {
      type: 'CLEAR_MESSAGE'
    }
  }
}
