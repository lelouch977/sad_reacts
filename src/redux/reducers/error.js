import {
  ERROR_401,
  ERROR
} from '../types'

const INITIAL_STATE = { data: null, texts: [], users: [], message: null }

module.exports = {
  error(state = INITIAL_STATE, action) {
    switch (action.type) {
    case ERROR_401:
      return { ...state, data: action.payload }

    case ERROR:
      return { ...state, data: action.payload }

    case 'ADD_TEXT':
      return { ...state, texts: [...state.texts, 'haha'] }

    case 'USER_LIST':
      return { ...state, users: action.payload }

    case 'message':
      console.log('received socket event')
      return { ...state, message: action.payload }

    case 'CLEAR_MESSAGE':
      return { ...state, message: null }

    default:
      return { ...state, data: null }
    }
  }
}
