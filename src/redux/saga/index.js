import { fork } from 'redux-saga/effects'
import sample from './sample'

export default function *root() {
  yield [
    fork(sample)
  ]
}
