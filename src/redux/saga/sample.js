import { call, put, takeLatest } from 'redux-saga/effects'
import axios from '../../utils/axios'
import {
  REQUESTED
} from '../types'

function *UserList() {
  let config = {
    method: 'get',
    url: 'http://localhost:3001/users'
  }

  try {
    const result = yield call(axios, config, 'USER_LIST')
    yield put(result)
  } catch (error) {
    console.error('UserList Error:', error)
  }
}

module.exports = function *() {
  yield takeLatest('USER_LIST' + REQUESTED, UserList)
}
