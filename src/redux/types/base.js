module.exports = {
  ERROR: 'ERROR',
  ERROR_400: 'ERROR_400',
  ERROR_401: 'ERROR_401',
  ERROR_500: 'ERROR_500',
  REQUESTED: '_REQUESTED'
}
