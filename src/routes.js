import React, { Component } from 'react'
import {
  Route,
  Link,
  Switch
} from 'react-router-dom'
import { connect } from 'react-redux'

import Index from './containers'
import Sample from './containers/sample.js'

const About = () => (
  <div>
    <h3>Welcome to the About Page</h3>
  </div>
)

const Topics = ({ match }) => (
  <div>
    <h3>Welcome to the Topics Page</h3>

    <ul>
      <li>
        <Link to={`${match.url}/culture`}>
          Culture
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/food`}>
          Food
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/travel`}>
          Travel
        </Link>
      </li>
    </ul>

    <Route path={`${match.url}/:topicId`} component={Topic}/>
    <Route exact path={match.url} render={() => (
      <div>
        <h3>Please select a topic.</h3>
        <p>{match.url}</p>
      </div>
    )}/>

  </div>
)

class Topics2 extends React.Component {
  render() {
    return (
      <div>
        <h3>Welcome to the Topics Page</h3>

        <ul>
          <li>
            <Link to={`${match.url}/culture`}>
              Culture
            </Link>
          </li>
          <li>
            <Link to={`${match.url}/food`}>
              Food
            </Link>
          </li>
          <li>
            <Link to={`${match.url}/travel`}>
              Travel
            </Link>
          </li>
        </ul>

        <Route path={`${match.url}/:topicId`} component={Topic}/>
        <Route exact path={match.url} render={() => (
          <div>
            <h3>Please select a topic.</h3>
            <p>{match.url}</p>
          </div>
        )}/>
      </div>
    )
  }
}

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
)


const NotFound = () => <h1>404.. This page is not found!</h1>

class App extends Component {
  render() {
    return (
      <div>
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/about'>About</Link></li>
          <li><Link to='/topics'>Topics</Link></li>
          <li><Link to='/sample'>Sample</Link></li>
        </ul>

        <hr/>
        <Switch>
          <Route exact path='/' component={Index} />
          <Route path='/about' component={About} />
          <Route path='/topics' component={Topics} />
          <Route path='/sample' component={Sample} />
        </Switch>
      </div>
    )
  }
}

export default App
