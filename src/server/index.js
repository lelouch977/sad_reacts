import express from 'express'
import http from 'http'
import socket_io from 'socket.io'
import path from 'path'

import handleRender from './handleRender.js'
import socket from './socket.js'

const app = express()
const server = http.createServer(app)
const io = socket_io().attach(server)

socket(io)

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, '../views'))

app.use(express.static('public'))
// app.get('*', function (req, res) {
//   res.render('sample', { title: 'Hey', message: 'Hello there!' })
// })
app.use(handleRender)

const PORT = process.env.PORT || 3000
server.listen(PORT, function () {
  console.log('app running @' + process.env.NODE_ENV + ' http://localhost:' + PORT)
})
