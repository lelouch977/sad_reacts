'use strict'
import moment from 'moment'

module.exports = {
  formatPhoneNumber: function (phone_number) {
    if (phone_number) {
      let formatted_number = phone_number.toString().replace(/[^0-9]/g, '')
      if (formatted_number.length > 2) {
        if (formatted_number.length < 6) {
          return formatted_number
        } else if (formatted_number.length > 5 && formatted_number.length < 10) {
          let first3 = formatted_number.substring(0, 3)
          let next3 = formatted_number.substring(3, 6)
          let nextSet3 = formatted_number.substring(6, formatted_number.length)

          if (formatted_number.length > 0) {
            formatted_number = first3 + '-' + next3 +
                (nextSet3 && nextSet3.length === 3 ? ('-' + nextSet3) : nextSet3)
          }
          return formatted_number
        }

        let first3 = formatted_number.substring(0, 3)
        let next3 = formatted_number.substring(3, 6)
        let next4 = formatted_number.substring(6, formatted_number.length)
        if (formatted_number.length > 0) {
          formatted_number = ('(' + first3 + ((first3.length >= 3) ? ') ' : '') + next3 +
            (((first3 + next3).length >= 6) ? '-' : '') + next4)
        }

        return formatted_number
      }

      return formatted_number
    }

    return phone_number
  },

  formatDate(date, format = 'MM/DD/YYYY') {
    if (!date) {
      return ''
    }
    return moment(date).format(format)
  },

  copyObjectValues(destination, source) {
    return Object.keys(destination).map((key) => {
      if (source && source[key]) {
        destination[key] = source[key]
      }
    })
  },

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  },

  titleCase(string) {
    string = string.split('_').join(' ')
    return string.toLowerCase().replace(/\b(\w)/g, s => s.toUpperCase())
  },

  generateUUID: function () {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  },

  sort(arr, key, ascending) {
    const list = arr.slice()
    const multiplier = ascending ? 1 : -1

    list.sort((prev, curr) => {
      const v1 = prev[key]
      const v2 = curr[key]

      if (typeof v1 === 'number') {
        return v1 < v2 ? 1 : -1
      }

      return v1.localeCompare(v2) * multiplier
    })

    return list
  },

  isEqual(obj1, obj2) {
    let isEqual = true
    Object.keys(obj1).map((index1) => {
      Object.keys(obj2).map((index2) => {
        if (index1 === index2 && obj1[index1] !== obj2[index2]) {
          isEqual = false
        }
      })
    })
    return isEqual
  },

  debouncer: (() => {
    let timer = 0
    return (callback, ms) => {
      clearTimeout(timer)
      timer = setTimeout(callback, ms)
    }
  })(),

  getPreviousLocation(router, count = 2) {
    return router.location.pathname.split('/').splice(0,
      router.location.pathname.split('/').length - count).join('/')
  },

  setNextLocation(router, entity, id) {
    let routerArray = router.location.pathname.split('/')
    if (routerArray.length > 0 && entity && routerArray[routerArray.length - 1] === id) {
      let newLocation = router.location.pathname
      newLocation = routerArray.splice(0, routerArray.length - 1).join('/')
      return `${newLocation}/${id}`
    }
    return router.location.pathname.includes(`/${entity}`)
      ? `${router.location.pathname}/${id}`
        : `${router.location.pathname}/${entity}/${id}`
  }
}
