import Validator from 'validator'

export default function validateInput(data, type) {
  let errors = {}
  let phone_pattern = new RegExp(/[\d]/g)

  if (type === 'Password') {
    if (Validator.isEmpty(data.password.trim())) {
      errors.password = 'Password is required'
    }
    if (!Validator.equals(data.password2.trim(), data.password.trim()) && !errors.password) {
      errors.password2 = 'Passwords do not match'
    }
    if (Validator.isEmpty(data.password2.trim())) {
      errors.password2 = 'Confirm your password'
    }
    if (!errors.password &&
      !Validator.matches(data.password.trim(), '(?=.*[0-9])(?=.*[A-Z]).{6,}$')) {
      errors.password = `Must be at least 6 characters, and contain 1 Numeric and 1 Uppercase`
    }
  }

  if (type === 'New' || type === 'Details') {
    if (data.status) {
      if (Validator.isEmpty(data.status.trim())) {
        errors.status = 'Status is required'
      }
    }
    if (Validator.isEmpty(data.first_name.trim())) {
      errors.first_name = 'First name is required'
    }
    if (Validator.isEmpty(data.last_name.trim())) {
      errors.last_name = 'Last name is required'
    }
    if (!Validator.isEmail(data.email.trim())) {
      errors.email = 'Email is invalid'
    }
    if (Validator.isEmpty(data.email.trim())) {
      errors.email = 'Email is required'
    }
    if (Validator.isEmpty(data.phone_number.toString().trim())) {
      errors.phone_number = 'Phone is required'
    } else if (!Validator.isLength(
      data.phone_number.toString().trim().match(phone_pattern).join(''),
     { min: 10, max: 10 })) {
      errors.phone_number = 'Phone number is invalid'
    }
    // if (Validator.isEmpty(data.address.trim())) {
    //   errors.address = 'Address is required'
    // }
    // if (Validator.isEmpty(data.city.trim())) {
    //   errors.city = 'City is required'
    // }
    // if (Validator.isEmpty(data.state.trim())) {
    //   errors.state = 'State is required'
    // }
    if (data.zip_code && !Validator.isLength(data.zip_code.toString().trim(), { min: 5, max: 5 })) {
      errors.zip_code = 'Zip Code is invalid'
    }
    // else if (Validator.isEmpty(data.zip_code.toString().trim())) {
    //   errors.zip_code = 'Zip Code is required'
    // }
  }

  return {
    errors,
    isValid: Object.keys(errors).length === 0
  }
}
