const webpack = require('webpack')
const path = require('path')

const build = path.resolve(__dirname, './public/js')
const js = path.resolve(__dirname, './src')

const config = {
  entry: {
    client: js + '/client.js',
    plugins: js + '/plugins'
  },
  output: {
    path: build,
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?/,
        include: js,
        loader: 'babel-loader',
        query: {
          presets: ['react']
        }
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
      }
    ]
  }
}

module.exports = config
